const BASE_URL = 'https://shopping-itclass.herokuapp.com/';

function getProducts() {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', BASE_URL.concat('products'));
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    let data = JSON.parse(xhr.responseText);
                    resolve(data);
                } else {
                    reject(xhr.status);
                }
            }
        }
        xhr.send();
    });
}
// Promise .then() .catch() .finally()

function drawList() {
    getProducts()
        .then(data => showList(data))
        .catch(err => console.log(err));
}

drawList();

function createCard(product) {
    let card = document.createElement('div');
    card.className = 'list-item';

    let info = document.createElement('div');
    info.className = 'info';

    let name = document.createElement('span');
    name.innerText = product.name;

    let count = document.createElement('span');
    count.innerText = product.count ? product.count : 1;

    let price = document.createElement('span');
    price.innerText = product.price + '$';

    let deleteBtn = document.createElement('button');
    deleteBtn.innerText = 'Delete';
    deleteBtn.addEventListener('click', () => deleteProduct(product._id));

    info.appendChild(name);
    info.appendChild(count);
    info.appendChild(price);

    card.appendChild(info);
    card.appendChild(deleteBtn);

    return card;
}

function showList(products) {
    let list = document.querySelector('.list');
    list.innerHTML = '';

    products.forEach(element => {
        let card = createCard(element);
        list.appendChild(card);
    });
}

function addProduct() {
    let name = document.querySelector('#name');
    let count = document.querySelector('#count');
    let price = document.querySelector('#price');

    if (name.value.length > 0 && count.value.length > 0 && price.value.length > 0) {
        let newProduct = {
            name: name.value,
            count: count.value,
            price: price.value
        };
        
        fetch(BASE_URL.concat('products'), {
            method: 'POST',
            body: JSON.stringify(newProduct),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(data => data.json())
            .then(_ => drawList());
    }
}

function deleteProduct(id) {
    fetch(BASE_URL.concat('product', `?id=${id}`), {
        method: 'DELETE'
    }).then(data => drawList());
}

document.querySelector('#add').addEventListener('click', addProduct);
